# == Author
# Glen Oglvie <gogilvie@oss.co.nz>
# Copyright 2017
# Username: nelg
# 
define peorchestration::memcache() {
  class { 'memcached':
    max_memory => '12%'
  }
  $url = "memcache://$fqdn/5555"
}
peorchestration::memcache Produces Memcache_server {
  url => $url
}

